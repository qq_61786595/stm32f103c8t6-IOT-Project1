//单片机头文件
#include "stm32f10x.h"

//硬件驱动
#include "KEY.h"
#include "LED.h"
#include "delay.h"
#include "usart.h"


//LED_INFO LED_info = {0};


/*
************************************************************
*	函数名称：	Beep_Init
*
*	函数功能：	蜂鸣器初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void KEY_Init(void)
{

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
	//GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource1);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource11);
	
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line11;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStructure);
	
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_Init(&NVIC_InitStructure);
}

/*
************************************************************
*	函数名称：	Beep_Set
*
*	函数功能：	蜂鸣器控制
*
*	入口参数：	status：开关蜂鸣器
*
*	返回参数：	无
*
*	说明：		开-BEEP_ON		关-BEEP_OFF
************************************************************
*/


void EXTI15_10_IRQHandler(void)
{
	if(EXTI_GetFlagStatus(EXTI_Line11) == SET)
	{
//		DelayXms(10);
//		if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11)==0)
//		{
//			if(LED_info.LED_Status==LED_ON) LED_Set(LED_OFF);
//			else LED_Set(LED_ON);
//		}
		if(LED_info.LED_Status==LED_ON) {
			LED_Set(LED_OFF);
			//UsartPrintf(USART_DEBUG, "%s\r\n",LED_info.LED_Status? "led_on":"led_off");
		}
		else{ 
			LED_Set(LED_ON);
			UsartPrintf(USART_DEBUG, "Connect MQTTs Server success\r\n");
			//UsartPrintf(USART_DEBUG, "%s\r\n",LED_info.LED_Status? "led_on":"led_off");
		}
		
		EXTI_ClearITPendingBit(EXTI_Line11);
	}
}
